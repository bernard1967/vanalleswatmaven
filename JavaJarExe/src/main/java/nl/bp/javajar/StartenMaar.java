package nl.bp.javajar;

public class StartenMaar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int getal1 = 0;
		int getal2 = 0;
		if(args[0] !=null && args[0].length() > 0 && args[1] != null && args[1].length() > 0) {
		
			try
			{
				getal1 = Integer.parseInt(args[0]);
				getal2 = Integer.parseInt(args[1]);
			}
			catch (NumberFormatException e) {
				System.out.println("args[0] is geen int of args[1] is geen int");
			}
		}
			
		System.out.println("uitkomst: " + getal1 + "+" + getal2 + "="  +
				new StartenMaar().displayOptelling(getal1, getal2));
	}
	
	public int displayOptelling(int getal1, int getal2)
	{
		return  (getal1 + getal2);
	}

}
